============
Struthon 0.7
============
Structural engineering design Python applications 

Changelog
---------
StruPy 0.7.3
  - port to python3, python 2 dropped
Struthon 0.6.4
  - open project website option added
Struthon 0.6.3
  - StruthonSteelMember hot fix
Struthon 0.6.2
  - new application StruthonSteelMember (buckling analysis available)
  - StruthonSteelMonoSection application removed
Struthon 0.5.4
  - ConcretePanel to Mecway7 upgraded
Struthon 0.5.2
  - ConcretePanel new features (cut_peak and smooth for reinforcement)
  - SteelMonoSection - profile class acc. EC3 included
  - SteelBrowser and SteelBoltedConnection updated
Struthon 0.5.1
  - StruthonSteelBoltedConnection added
Struthon 0.4.5.2
  - StruthonConcretePanel save-open corrected
Struthon 0.4.5
  - StruthonConcretePanel Mecway integrated  
Struthon 0.4.4
  - StruthonConcretePanel new features (dxf export, multi loadcase, save/open project file)  
Struthon 0.4.2
  - StruthonConcretePanel upgraded  
Struthon 0.4.1
  - StruthonConcretePanel application added  
Struthon 0.3.3
  - StruthonConcreteMonoSection user interface upgraded
  - StruthonConcreteMonoSection M-N chart creating speed improved  
Struthon 0.3.2
  - no changes  
Struthon 0.3.1
  - StruthonSteelSectionBrowser application upgraded (section drawing, 
    section groups filter)
  - StruthonSteelMonoSection application added
Struthon 0.2
  - StruthonSteelSectionBrowser application added
Struthon 0.1
  - the first working version with StruthonConcreteMonoSection application 

Requirements
------------
Struthon is based on Python 3.6 and few non-standard Python library:

  - StruPy (https://pypi.python.org/pypi/strupy)
  - NumPy (http://www.numpy.org)
  - PyQt5 (https://www.riverbankcomputing.com/software/pyqt)
  - Matplotlib (http://matplotlib.org)
  - Unum (https://pypi.python.org/pypi/Unum)
  - xlrd (https://pypi.python.org/pypi/xlrd)
  - dxfwrite (https://pypi.python.org/pypi/dxfwrite)
  - easygui (https://pypi.python.org/pypi/easygui)
  - pyautocad (https://pypi.python.org/pypi/pyautocad)

How to install
--------------
After the Python and needed library was installed, install Struthon by typing::

    pip install struthon

You can also find more install information at project website.

Windows 7,10 and Linux Xubutu tested.

Licence
-------
Struthon is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

Copyright (C) 2015-2020 Lukasz Laba <lukaszlaba@gmail.com>

Contributions
-------------
If you want to help out, create a pull request or write email.

More information
----------------
Project website: https://bitbucket.org/struthonteam/struthon

Code repository: https://bitbucket.org/struthonteam/struthon

PyPI package: https://pypi.python.org/pypi/struthon

Contact: Lukasz Laba <lukaszlaba@gmail.com>